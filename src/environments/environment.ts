// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC5Fk4Hy4Mkpqhy7-344oi7BBMHvqDrisU",
    authDomain: "fitness-tracker-c9bc6.firebaseapp.com",
    databaseURL: "https://fitness-tracker-c9bc6.firebaseio.com",
    projectId: "fitness-tracker-c9bc6",
    storageBucket: "fitness-tracker-c9bc6.appspot.com",
    messagingSenderId: "262410994167"
  }
};
