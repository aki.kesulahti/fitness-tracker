import { NgModule } from '@angular/core';
import {  MatButtonModule,
          MatDatepickerModule,
          MatNativeDateModule,
          MatSidenavModule,
          MatToolbarModule,
          MatListModule,
          MatTabsModule,
          MatCheckboxModule,
          MatCardModule,
          MatSelectModule,
          MatProgressSpinnerModule,
          MatDialogModule,
          MatTableModule,
          MatSortModule,
          MatPaginatorModule,
          MatSnackBarModule
        } from '@angular/material';
import { MatIconModule } from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatGridListModule} from '@angular/material/grid-list';


@NgModule({
  imports: [
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatGridListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatTabsModule,
    MatCardModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatSnackBarModule
    ],
  exports: [
      MatButtonModule,
      MatCheckboxModule,
      MatIconModule,
      MatInputModule,
      MatFormFieldModule,
      MatGridListModule,
      MatDatepickerModule,
      MatNativeDateModule,
      MatSidenavModule,
      MatToolbarModule,
      MatListModule,
      MatTabsModule,
      MatCardModule,
      MatSelectModule,
      MatProgressSpinnerModule,
      MatDialogModule,
      MatTableModule,
      MatSortModule,
      MatPaginatorModule,
      MatSnackBarModule
    ],
})
export class MaterialModule { }
